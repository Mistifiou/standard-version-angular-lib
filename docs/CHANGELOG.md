# [0.2.0](https://gitlab.com/Mistifiou/standard-version-angular-lib/compare/v0.1.0...v0.2.0) (2020-03-28)


### Bug Fixes

* a ([b056017](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/b0560171d500ad6114168c741c76b4df75bc8394))
* a fix test ([0e799dd](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/0e799dd666f6cb008a8b876568659cc2a160ef38))
* add gitignore ([8bcedc3](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/8bcedc3966fda0dd5c04e289a0093b98c2f21986))
* fix ([46292a3](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/46292a3af615c7ddcb56dd7ff910a08462dd29a7))
* j ([477d142](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/477d1421e18780d2e71ff590a36488044f16de86))
* test ([99b9e77](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/99b9e775018f493c3ecb33360779ba7efeb8cb6b))
* test ([0dedd77](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/0dedd7750a7533baa841ada4c8348890d3662f2c))
* test ([d8879b7](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/d8879b784946748176bdfcf2ac617afa12143bd9))
* test ([bb2ec4c](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/bb2ec4c921466fd95cffa2eeb9ec4c3059c03f76))
* **x:** s ([a1c08ac](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/a1c08ac8ef555ceb695c844f53bff61d57b34013))
* test ([1428507](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/1428507f5a24e64f826eda474c7d17688135a186))
* **lerna:** fix lerna npm auth ([913ccad](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/913ccada28122a8a7da007b9803917d6cdc0b4be))


### Features

* a ([cdbe706](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/cdbe706db5d46c55358784f2409c3ca6c06de062))
* a new feature ([c81a77b](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/c81a77b602d11688d8af80c217fc364852305e31))
* a new feature ([d779e20](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/d779e20b953921746d0d029d75ca7e006fd2fa3f))
* a new feature ([dc88887](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/dc888870e9942624b34b61674ded8eee7d5ba165))
* some new feature ([adb82ad](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/adb82aded04d91b03bbc8622aac0aa6b47ee075d))
* some new feature ([d067238](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/d067238ecc9f3d89a0c0efb9f6419f576acaa856))
* some test local ([7976d05](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/7976d0567506016fa892f4fa2eb394d1be7a2fef))
* test ([43ad19b](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/43ad19b0ef1dcc27aa933157a7ebafdad3b22799))
* test ([7c52363](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/7c5236333edb7c329792f67a9d1b69b333d02796))
* test ([aae0f0f](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/aae0f0fecf3ba3cc31ee1e4eb781dc58713d5f80))
* test ([2bcf171](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/2bcf17133d737c463332e207779a6f465f799802))
* test ([cfa340a](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/cfa340ab5662fd2f1a8745aae42ed97ce089569b))
* test ([7e04d01](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/7e04d01a8eff13ae9714a1314b33f36109f47616))
* test ([679a0f3](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/679a0f34e04d186896460974d596e67910889e5d))
* test job ([3f925dd](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/3f925dddaf48e5ce6321a18cc90a148003c2e626))
* une question d'ordre ([0ebbe22](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/0ebbe22c293929a5b6f560dfa49ed412a5422ec6))
* **ci:** add publish with lerna in ci ([dcef6ab](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/dcef6ab084a0ddc87ef307297fb098e600205988))
* **projects:** make some updates to publish ([2c2bf76](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/2c2bf76c3e24d86e149205de71a3897e739afa05))

# [1.0.0-beta.3](https://gitlab.com/Mistifiou/standard-version-angular-lib/compare/v1.0.0-beta.2...v1.0.0-beta.3) (2020-03-28)


### Bug Fixes

* a ([b056017](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/b0560171d500ad6114168c741c76b4df75bc8394))
* a fix test ([0e799dd](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/0e799dd666f6cb008a8b876568659cc2a160ef38))
* add gitignore ([8bcedc3](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/8bcedc3966fda0dd5c04e289a0093b98c2f21986))
* fix ([46292a3](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/46292a3af615c7ddcb56dd7ff910a08462dd29a7))
* j ([477d142](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/477d1421e18780d2e71ff590a36488044f16de86))
* test ([99b9e77](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/99b9e775018f493c3ecb33360779ba7efeb8cb6b))
* test ([0dedd77](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/0dedd7750a7533baa841ada4c8348890d3662f2c))
* test ([d8879b7](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/d8879b784946748176bdfcf2ac617afa12143bd9))
* test ([bb2ec4c](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/bb2ec4c921466fd95cffa2eeb9ec4c3059c03f76))
* **ci:** fix ci and add some stuff ([e57638b](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/e57638b2f4d1eb4a998caf30c73b03c3c57224a8))
* **ci:** fix ci feature release step ([c420eeb](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/c420eeb2815ac397707772051af71f4a32362dfc))
* **ci:** fix ci feature step ([b7b733b](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/b7b733b6a2a0d80a4ab43b15921aed77eefeee8f))
* **ci:** try another config ([56fe1f9](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/56fe1f90516e59a2c67c99de8091c0f2a4dc5f1e))
* **x:** s ([a1c08ac](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/a1c08ac8ef555ceb695c844f53bff61d57b34013))
* test ([1428507](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/1428507f5a24e64f826eda474c7d17688135a186))
* **lerna:** fix lerna npm auth ([913ccad](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/913ccada28122a8a7da007b9803917d6cdc0b4be))


### Features

* a ([cdbe706](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/cdbe706db5d46c55358784f2409c3ca6c06de062))
* a new feature ([c81a77b](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/c81a77b602d11688d8af80c217fc364852305e31))
* a new feature ([d779e20](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/d779e20b953921746d0d029d75ca7e006fd2fa3f))
* a new feature ([dc88887](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/dc888870e9942624b34b61674ded8eee7d5ba165))
* some new feature ([adb82ad](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/adb82aded04d91b03bbc8622aac0aa6b47ee075d))
* some new feature ([d067238](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/d067238ecc9f3d89a0c0efb9f6419f576acaa856))
* some test local ([7976d05](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/7976d0567506016fa892f4fa2eb394d1be7a2fef))
* test ([43ad19b](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/43ad19b0ef1dcc27aa933157a7ebafdad3b22799))
* test ([7c52363](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/7c5236333edb7c329792f67a9d1b69b333d02796))
* test ([aae0f0f](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/aae0f0fecf3ba3cc31ee1e4eb781dc58713d5f80))
* test ([2bcf171](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/2bcf17133d737c463332e207779a6f465f799802))
* test ([cfa340a](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/cfa340ab5662fd2f1a8745aae42ed97ce089569b))
* test ([7e04d01](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/7e04d01a8eff13ae9714a1314b33f36109f47616))
* test ([679a0f3](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/679a0f34e04d186896460974d596e67910889e5d))
* test job ([3f925dd](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/3f925dddaf48e5ce6321a18cc90a148003c2e626))
* une question d'ordre ([0ebbe22](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/0ebbe22c293929a5b6f560dfa49ed412a5422ec6))
* **ci:** add lerna ([92e4e7a](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/92e4e7a0052aabfaa77aadc2eef430daa4b4a836))
* **ci:** add publish with lerna in ci ([dcef6ab](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/dcef6ab084a0ddc87ef307297fb098e600205988))
* **ci:** add some branch configuration ([bd9552f](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/bd9552fb19210f9f73f05027bee49efb037ce178)), closes [#2](https://gitlab.com/Mistifiou/standard-version-angular-lib/issues/2)
* **project:** add test project ([beff141](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/beff141233db8c4ba8ef14fc4368b7d114b5f850))
* **projects:** make some updates to publish ([2c2bf76](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/2c2bf76c3e24d86e149205de71a3897e739afa05))

# [1.0.0-alpha.17](https://gitlab.com/Mistifiou/standard-version-angular-lib/compare/v1.0.0-alpha.16...v1.0.0-alpha.17) (2020-03-28)


### Bug Fixes

* a ([b056017](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/b0560171d500ad6114168c741c76b4df75bc8394))
* a fix test ([0e799dd](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/0e799dd666f6cb008a8b876568659cc2a160ef38))
* add gitignore ([8bcedc3](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/8bcedc3966fda0dd5c04e289a0093b98c2f21986))
* fix ([46292a3](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/46292a3af615c7ddcb56dd7ff910a08462dd29a7))
* j ([477d142](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/477d1421e18780d2e71ff590a36488044f16de86))
* test ([99b9e77](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/99b9e775018f493c3ecb33360779ba7efeb8cb6b))
* test ([0dedd77](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/0dedd7750a7533baa841ada4c8348890d3662f2c))
* test ([d8879b7](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/d8879b784946748176bdfcf2ac617afa12143bd9))
* test ([bb2ec4c](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/bb2ec4c921466fd95cffa2eeb9ec4c3059c03f76))
* **x:** s ([a1c08ac](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/a1c08ac8ef555ceb695c844f53bff61d57b34013))


### Features

* a ([cdbe706](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/cdbe706db5d46c55358784f2409c3ca6c06de062))
* some test local ([7976d05](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/7976d0567506016fa892f4fa2eb394d1be7a2fef))
* test ([43ad19b](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/43ad19b0ef1dcc27aa933157a7ebafdad3b22799))

# [1.0.0-alpha.16](https://gitlab.com/Mistifiou/standard-version-angular-lib/compare/v1.0.0-alpha.15...v1.0.0-alpha.16) (2020-03-28)


### Features

* une question d'ordre ([0ebbe22](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/0ebbe22c293929a5b6f560dfa49ed412a5422ec6))

# [1.0.0-alpha.15](https://gitlab.com/Mistifiou/standard-version-angular-lib/compare/v1.0.0-alpha.14...v1.0.0-alpha.15) (2020-03-28)


### Features

* test ([7c52363](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/7c5236333edb7c329792f67a9d1b69b333d02796))

# [1.0.0-alpha.14](https://gitlab.com/Mistifiou/standard-version-angular-lib/compare/v1.0.0-alpha.13...v1.0.0-alpha.14) (2020-03-28)


### Features

* test ([aae0f0f](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/aae0f0fecf3ba3cc31ee1e4eb781dc58713d5f80))

# [1.0.0-alpha.13](https://gitlab.com/Mistifiou/standard-version-angular-lib/compare/v1.0.0-alpha.12...v1.0.0-alpha.13) (2020-03-28)


### Features

* test job ([3f925dd](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/3f925dddaf48e5ce6321a18cc90a148003c2e626))

# [1.0.0-alpha.12](https://gitlab.com/Mistifiou/standard-version-angular-lib/compare/v1.0.0-alpha.11...v1.0.0-alpha.12) (2020-03-28)


### Features

* a new feature ([c81a77b](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/c81a77b602d11688d8af80c217fc364852305e31))
* a new feature ([d779e20](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/d779e20b953921746d0d029d75ca7e006fd2fa3f))

# [1.0.0-alpha.2](https://gitlab.com/Mistifiou/standard-version-angular-lib/compare/v1.0.0-alpha.1...v1.0.0-alpha.2) (2020-03-28)


### Features

* **ci:** add lerna ([92e4e7a](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/92e4e7a0052aabfaa77aadc2eef430daa4b4a836))
* **ci:** add publish with lerna in ci ([dcef6ab](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/dcef6ab084a0ddc87ef307297fb098e600205988))
* **project:** add test project ([beff141](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/beff141233db8c4ba8ef14fc4368b7d114b5f850))
* **projects:** make some updates to publish ([2c2bf76](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/2c2bf76c3e24d86e149205de71a3897e739afa05))

# 1.0.0-alpha.1 (2020-03-28)


### Bug Fixes

* **ci:** add branch config + fix gitlab-ci.yml :latest ([71862a6](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/71862a67e9927fd627670e2662ebf7cb7dbd4790))
* **ci:** fix ci and add some stuff ([e57638b](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/e57638b2f4d1eb4a998caf30c73b03c3c57224a8))
* **ci:** fix ci feature release step ([c420eeb](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/c420eeb2815ac397707772051af71f4a32362dfc))
* **ci:** fix ci feature step ([b7b733b](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/b7b733b6a2a0d80a4ab43b15921aed77eefeee8f))
* **ci:** node lastest needed ([c57d4e5](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/c57d4e5c6eb846753f8cad217b7cfb66f56f4378))
* **ci:** try another config ([56fe1f9](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/56fe1f90516e59a2c67c99de8091c0f2a4dc5f1e))
* **project:** add release command ([77f2508](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/77f25089715654157ef620269e76264d6d5c352c))
* **semantic-release:** add ci configuration ([73cebc5](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/73cebc59228ea8551107dcef470add30e9489f19))
* **semantic-release:** fix ci error ([9acfee6](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/9acfee640de534f707f7ea6c138b2cf600eb0817))


### Features

* **ci:** add some branch configuration ([bd9552f](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/bd9552fb19210f9f73f05027bee49efb037ce178)), closes [#2](https://gitlab.com/Mistifiou/standard-version-angular-lib/issues/2)
* **project:** -_- ([50d4747](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/50d47476e42faad2c4f0a00e8bf415fb7f27d7d5))
* **project:** add commitizen ([7dc4aa8](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/7dc4aa848def8acf2fc71c47e37570fb612e8e2e))
* **project:** add semantic-release ([6adbcf8](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/6adbcf85c1b83bafa6e486bf5851ecd61d1f6f63))
* **project:** ci configuration ([5db2c61](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/5db2c61044b6c8a564da5fc67d6e38bde7da5712))
* **project:** dependency gui + upgrade command ([b65bce8](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/b65bce84e9ab8072fa90639593c59f25ce5cdfcc))

# [1.0.0-beta.2](https://gitlab.com/Mistifiou/standard-version-angular-lib/compare/v1.0.0-beta.1...v1.0.0-beta.2) (2020-03-28)


### Bug Fixes

* **semantic-release:** fix ci error ([9acfee6](https://gitlab.com/Mistifiou/standard-version-angular-lib/commit/9acfee640de534f707f7ea6c138b2cf600eb0817))
