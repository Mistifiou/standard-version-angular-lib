/*
 * Public API Surface of test-lib-bis
 */

export * from './lib/test-lib-bis.service';
export * from './lib/test-lib-bis.component';
export * from './lib/test-lib-bis.module';
