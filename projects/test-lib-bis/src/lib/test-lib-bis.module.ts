import { NgModule } from '@angular/core';
import { TestLibBisComponent } from './test-lib-bis.component';



@NgModule({
  declarations: [TestLibBisComponent],
  imports: [
  ],
  exports: [TestLibBisComponent]
})
export class TestLibBisModule { }
