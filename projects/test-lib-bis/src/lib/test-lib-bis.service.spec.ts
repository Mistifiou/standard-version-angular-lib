import { TestBed } from '@angular/core/testing';

import { TestLibBisService } from './test-lib-bis.service';

describe('TestLibBisService', () => {
  let service: TestLibBisService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestLibBisService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
