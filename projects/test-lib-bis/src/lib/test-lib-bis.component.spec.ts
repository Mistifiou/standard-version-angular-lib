import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestLibBisComponent } from './test-lib-bis.component';

describe('TestLibBisComponent', () => {
  let component: TestLibBisComponent;
  let fixture: ComponentFixture<TestLibBisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestLibBisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestLibBisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
