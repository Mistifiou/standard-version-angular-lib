# Standard version lib

Projet POC permettant de tester un socle technique équipé d'outils pour:
- La gestion des versions
- La gestion des dépendances

[changelog](./docs/changelog.md)

# Spécifications

[Conventional commits](https://www.conventionalcommits.org/en/v1.0.0/)  
[Sementic versioning](https://semver.org/)

# Outils

## [NPM-GUI](https://www.npmjs.com/package/npm-gui)

Utilisable via la commande `npm run gui`.  
Permet de visualiser les dépendances

## [npm-upgrade](https://www.npmjs.com/package/npm-upgrade)

Utilisable via la commande `npm run upgrade`.
Permet de mettre à jour les dépendances du projet  

## [client angular](https://update.angular.io/)

Le client angular sert notemment dans ce contexte à la montée de version de Angular sur le projet.

## [commitizen](https://www.npmjs.com/package/commitizen)

Permet de normaliser les messages de commit.  

Pour l'utiliser au sein du projet, faire `npm run commit` au lieux de `git commit`

## [Sementic release](https://github.com/semantic-release/semantic-release)

Sementic release va se charger d'effectuer les montées de version à partir du message de commit.

[La liste des plugins](https://github.com/semantic-release/semantic-release/blob/master/docs/extending/plugins-list.md) permettant d'agir lors d'un déploiement est vraiment importante.  

